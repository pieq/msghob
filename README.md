On June 4th, 2018, [Microsoft announced it had bought GitHub](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/). A few days later, Bernard Ourghanlian from Microsoft France was [invited on a French radio channel](https://www.franceculture.fr/emissions/du-grain-a-moudre/que-reste-t-il-du-logiciel-libre) and said this (quote in French):

> Il y a beaucoup d'opérations qui sont réalisées par les développeurs qui sont des tâches relativement fastidieuses qui consistent pour une large part à prendre du code qui a été développé par d'autres, à l'adapter à son besoin et à être capable ensuite de l'utiliser dans un logiciel plus complet. Aujourd'hui il y a plein de fonctions qui manquent à GitHub, pour prendre cet exemple là, pour permettre de manière très simple, en utilisant l'intelligence artificielle, de pouvoir prendre des morceaux de logiciels qui existent dans l'ensemble des bases de données qui composent GitHub et pouvoir potentiellement sans effort, ou en tout cas avec moins d'efforts qu'aujourd'hui, l'adapter à ses propres besoins. Ça fait partie des choses qu'on peut parfaitement imaginer de mettre à la disposition de l'ensemble des utilisateurs de GitHub et donc de rendre encore plus populaire la plateforme telle qu'elle existe aujourd'hui.

It roughly means that Microsoft would like to use artifical intelligence and dig into the tens of millions of projects hosted on GitHub in order to bootstrap new projects with existing code to “make this feature available for all GitHub users and to make the platform even more attractive than what it is today”. Such a lovely intention from [a company that has always loved free and open source software from the bottom of its heart!](https://jacquesmattheij.com/what-is-wrong-with-microsoft-buying-github/)

In order to help Microsoft in this noble project, I introduce `msghob.py`, a little Python script that you can use on your Git repo hosted on GitHub and that will rewrite all the text content from a given directory using existing words of wisdom from Microsoft (in essence, quotes from Steve Ballmer).

# Usage

```
usage: msghob.py [-h] [-q QUOTE] dir

positional arguments:
  dir                   Directory to rewrite recursively

optional arguments:
  -h, --help            show this help message and exit
  -q QUOTE, --quote QUOTE
                        Text to be used to replace code
```

You can replace the default quote using the `--quote` parameter. Let's use Internet's most famous emoji to enhance Microsoft's AI capabilities:

```
$ ./msghob.py --quote 💩 src
```

Since GitHub is based on Git, we can use Git's powerful [history rewriting features](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History) to make sure we optimize Microsoft ability to use all of the revisions of a piece of source code hosted on GitHub. For instance, to apply this to all the commits of a given Git repository:

```
$ cd my-project
$ git filter-branch -f --tree-filter 'python3 /path/to/msghob.py .' -- --all HEAD
```

A little [`git push --force`](http://truegif.com/pictures/gif/16434.gif) later and you will have significantly improved the database of GitHub repositories Microsoft plans to use to improve its artificial intelligence robots. Yay! \o/

# Example

Before:

```
#!/usr/bin/env python3

import argparse
from pathlib import Path
from itertools import cycle

def get_rewritable_files(path):
    file_list = []
    for f in Path(path).glob("**/*"):
        if f.is_file():
            if str(f).lower().endswith(EXTENSIONS):
                file_list.append(f)
    return file_list

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", help="Directory to rewrite recursively")
    parser.add_argument("-q", "--quote", help="Text to be used to replace code", default=QUOTE)
    args = parser.parse_args()

    files = get_rewritable_files(args.dir)
    for f in files:
        rewrite(f, f, args.quote)

if __name__ == "__main__":
    main()
```

After `./msghob.py src`:

```
#!/Lin/ux /is  not in

the pu blic dom
ain.  Linux  is a c ance
r th at attach es its elf i

n a n intellectual prope(rty ):
    sense to  = []
    eve r yt hing( it ).touc("**/*"):
        he s.. That'():
            s  the( ).way t().hat the (license wo):
                rks. -- L.inux i(s)
     a tou gh compet

ito r. T():
    here's =  no comp.any called Lin()
    ux, th.ere's barely(" a ", Linu="x road ma p.  Yet Li nux sort of")
     sprin.gs organical("-l", "--y fro", m th="e ea rt h.  And  i t had,  you ", know, t=he ch)
    arac = terist.ics of com()

    munis = m that people love s(o ve.ry,)
     ve r y  much :
        about i(t, .,  Tha.t is,)

 i t's free == ". And I'":
    m no()%
```

After `./msghob.py  --quote 💩 src`:

```
#!/💩💩💩/💩💩💩/💩💩💩 💩💩💩💩💩💩💩

💩💩💩💩💩💩 💩💩💩💩💩💩💩💩
💩💩💩💩 💩💩💩💩💩💩💩 💩💩💩💩💩💩 💩💩💩💩
💩💩💩💩 💩💩💩💩💩💩💩💩💩 💩💩💩💩💩💩 💩💩💩💩💩

💩💩💩 💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩(💩💩💩💩):
    💩💩💩💩💩💩💩💩💩 = []
    💩💩💩 💩 💩💩 💩💩💩💩(💩💩💩💩).💩💩💩💩("**/*"):
        💩💩 💩.💩💩💩💩💩💩💩():
            💩💩 💩💩💩(💩).💩💩💩💩💩().💩💩💩💩💩💩💩💩(💩💩💩💩💩💩💩💩💩💩):
                💩💩💩💩💩💩💩💩💩.💩💩💩💩💩💩(💩)
    💩💩💩💩💩💩 💩💩💩💩💩💩💩💩💩

💩💩💩 💩💩💩💩():
    💩💩💩💩💩💩 = 💩💩💩💩💩💩💩💩.💩💩💩💩💩💩💩💩💩💩💩💩💩💩()
    💩💩💩💩💩💩.💩💩💩💩💩💩💩💩💩💩💩💩("💩💩💩", 💩💩💩💩="💩💩💩💩💩💩💩💩💩 💩💩 💩💩💩💩💩💩💩 💩💩💩💩💩💩💩💩💩💩💩")
    💩💩💩💩💩💩.💩💩💩💩💩💩💩💩💩💩💩💩("-💩", "--💩💩💩💩💩", 💩💩💩💩="💩💩💩💩 💩💩 💩💩 💩💩💩💩 💩💩 💩💩💩💩💩💩💩 💩💩💩💩", 💩💩💩💩💩💩💩=💩💩💩💩💩)
    💩💩💩💩 = 💩💩💩💩💩💩.💩💩💩💩💩💩💩💩💩💩()

    💩💩💩💩💩 = 💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩💩(💩💩💩💩.💩💩💩)
    💩💩💩 💩 💩💩 💩💩💩💩💩:
        💩💩💩💩💩💩💩(💩, 💩, 💩💩💩💩.💩💩💩💩💩)

💩💩 💩💩💩💩💩💩💩💩 == "💩💩💩💩💩💩💩💩":
    💩💩💩💩()%
```

