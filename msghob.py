#!/usr/bin/env python3
#
# Copyright 2018 Pierre Equoy
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 

import argparse
from pathlib import Path
from itertools import cycle

CHAR_TO_KEEP = " .,;:*+-=!?#$%&\"'/(){}[]<>\t"

QUOTE = ("Linux is not in the public domain. Linux is a cancer that attaches "
         "itself in an intellectual property sense to everything it touches. "
         "That's the way that the license works. -- "
         "Linux is a tough competitor. There's no company called Linux, there's "
         "barely a Linux road map. Yet Linux sort of springs organically from "
         "the earth. And it had, you know, the characteristics of communism "
         "that people love so very, very much about it. That is, it's free. And "
         "I'm not trying to make fun of it, because it's a real competitive "
         "issue. Today, I would say, we still don't see a lot of Linux "
         "competition in most quarters on the desktop, and we see a lot of Linux "
         "competition in some server markets. And we could either say, hey, Linux "
         "is going to roll over the world, but I don't see that happening. That's "
         "not what's going on right now. ")

EXTENSIONS = (".asm", ".asp", ".aspx", ".c", ".cbl", ".cfg", ".cl", ".clj", ".cob",
              ".cpp", ".cs", ".css", ".erl", ".f", ".f90", ".for", ".fs", ".go",
              ".h", ".hs", ".htm", ".html", ".ini", ".java", ".js", ".json", ".jsp",
              ".lisp", ".lsp", ".lua", ".md", ".ml", ".pas", ".php", ".pl", ".po",
              ".pp", ".py", ".qml", ".r", ".rb", ".rs", ".rst", ".sc", ".scala",
              ".sh", ".sql", ".swift", ".toml", ".txt", ".vala", ".vbs", ".xhtml",
              ".xml", ".yaml", ".yml")

def rewrite(finput, foutput, quote):
    quote_iterator = cycle(quote)
    translated = []
    with open(finput, "r", errors="replace") as f:
        code = f.readlines()
        for line in code:
            line_translated = ""
            if line != "\n":
                for c in line:
                        if c in CHAR_TO_KEEP:
                                line_translated += c
                        elif c != "\n":
                                line_translated += next(quote_iterator)
            translated.append(line_translated)

    with open(foutput, 'w') as f:
        f.write("\n".join(translated))

def get_rewritable_files(path):
    file_list = []
    for f in Path(path).glob("**/*"):
        if f.is_file():
            if str(f).lower().endswith(EXTENSIONS):
                file_list.append(f)
    return file_list

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", help="Directory to rewrite recursively")
    parser.add_argument("-q", "--quote", help="Text to be used to replace code", default=QUOTE)
    args = parser.parse_args()

    files = get_rewritable_files(args.dir)
    for f in files:
        rewrite(f, f, args.quote)

if __name__ == "__main__":
    main()
